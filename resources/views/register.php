<!DOCTYPE html>
<html>
<head>
	<title>Form Sign Up</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>
	<h2>Sign Up Form</h2>
	<p>First name:</p>
	<input type="text" name="">
	<p>Last name:</p>
	<input type="text" name="">

	<p>Gender:</p>
	<input type="radio" id="Male" name="jenis_kelamin"><label for="Male">Male</label>
	<br>
	<input type="radio" id="Female" name="jenis_kelamin"><label for="Female">Female</label>
	<br>
	<input type="radio" id="Other" name="jenis_kelamin"><label for="Other">Other</label>
	<br>
	<p>Nationality</p>
	<label for="cbx">
		<select id="cbx">
			<option>Indonesian</option>
			<option>Singapuran</option>
			<option>Malaysian</option>
			<option>Australian</option>
		</select>
	<p>Language Spoken:</p>
	<input type="checkbox" id="bahasaindonesia"><label for="bahasaindonesia">Bahasa Indonesia</label>
	<br>
	<input type="checkbox" id="bahasainggris"><label for="bahasainggris">Bahasa Inggris</label>
	<br>
	<input type="checkbox" id="bahasalainnya"><label for="bahasalainnya">Other</label>
	<br>
	<p>Bio</p>
	<textarea cols="30" rows="10"></textarea>
	<br>
	<button type="submit">Sign Up</button>
</body>
</html>